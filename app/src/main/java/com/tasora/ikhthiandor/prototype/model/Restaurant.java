package com.tasora.ikhthiandor.prototype.model;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class Restaurant {

    @Expose
    private String address;
    @Expose
    private String city;
    @SerializedName("closing-hour")
    @Expose
    private Integer closingHour;
    @Expose
    private String id;
    @Expose
    private com.tasora.ikhthiandor.prototype.model.Latlng latlng;
    @Expose
    private Menu menu;
    @SerializedName("open-24-hours")
    @Expose
    private Boolean open24Hours;
    @SerializedName("opening-hour")
    @Expose
    private Integer openingHour;
    @SerializedName("phone_number")
    @Expose
    private String phoneNumber;
    @SerializedName("restaurant_name")
    @Expose
    private String restaurantName;
    @Expose
    private String status;

    /**
     * @return The address
     */
    public String getAddress() {
        return address;
    }

    /**
     * @param address The address
     */
    public void setAddress(String address) {
        this.address = address;
    }

    /**
     * @return The city
     */
    public String getCity() {
        return city;
    }

    /**
     * @param city The city
     */
    public void setCity(String city) {
        this.city = city;
    }

    /**
     * @return The closingHour
     */
    public Integer getClosingHour() {
        return closingHour;
    }

    /**
     * @param closingHour The closing-hour
     */
    public void setClosingHour(Integer closingHour) {
        this.closingHour = closingHour;
    }

    /**
     * @return The id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id The id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return The latlng
     */
    public com.tasora.ikhthiandor.prototype.model.Latlng getLatlng() {
        return latlng;
    }

    /**
     * @param latlng The latlng
     */
    public void setLatlng(com.tasora.ikhthiandor.prototype.model.Latlng latlng) {
        this.latlng = latlng;
    }

    /**
     * @return The menu
     */
    public Menu getMenu() {
        return menu;
    }

    /**
     * @param menu The menu
     */
    public void setMenu(Menu menu) {
        this.menu = menu;
    }

    /**
     * @return The open24Hours
     */
    public Boolean getOpen24Hours() {
        return open24Hours;
    }

    /**
     * @param open24Hours The open-24-hours
     */
    public void setOpen24Hours(Boolean open24Hours) {
        this.open24Hours = open24Hours;
    }

    /**
     * @return The openingHour
     */
    public Integer getOpeningHour() {
        return openingHour;
    }

    /**
     * @param openingHour The opening-hour
     */
    public void setOpeningHour(Integer openingHour) {
        this.openingHour = openingHour;
    }

    /**
     * @return The phoneNumber
     */
    public String getPhoneNumber() {
        return phoneNumber;
    }

    /**
     * @param phoneNumber The phone_number
     */
    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    /**
     * @return The restaurantName
     */
    public String getRestaurantName() {
        return restaurantName;
    }

    /**
     * @param restaurantName The restaurant_name
     */
    public void setRestaurantName(String restaurantName) {
        this.restaurantName = restaurantName;
    }

    /**
     * @return The status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status The status
     */
    public void setStatus(String status) {
        this.status = status;
    }
}
