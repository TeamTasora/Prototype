package com.tasora.ikhthiandor.prototype.api;

import com.tasora.ikhthiandor.prototype.model.Restaurant;

import java.util.List;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.http.GET;

/**
 * Created by ikhthiandor on 7/26/15.
 */
public class TasoraApiClient {
    private static TasoraApiInterface tasoraService;


    public static TasoraApiInterface getTasoraApiClient() {

        if(tasoraService == null) {

            RestAdapter restAdapter = new RestAdapter.Builder()
                    .setLogLevel(RestAdapter.LogLevel.FULL)
                    .setEndpoint("http://minhazr.koding.io:5000")
                    .build();
            tasoraService = restAdapter.create(TasoraApiInterface.class);
        }

        return tasoraService;
    }


    public interface TasoraApiInterface {

        @GET("/restaurants?status=open")

        void listRestaurants(Callback<List<Restaurant>> cb);

    }



}
