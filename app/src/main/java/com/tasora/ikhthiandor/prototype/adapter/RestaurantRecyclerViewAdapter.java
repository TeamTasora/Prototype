package com.tasora.ikhthiandor.prototype.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.tasora.ikhthiandor.prototype.MapsActivity;
import com.tasora.ikhthiandor.prototype.R;
import com.tasora.ikhthiandor.prototype.model.Latlng;
import com.tasora.ikhthiandor.prototype.model.Restaurant;

import java.util.ArrayList;

/**
 * Created by ikhthiandor on 7/26/15.
 */
public class RestaurantRecyclerViewAdapter extends RecyclerView.Adapter<RestaurantRecyclerViewAdapter.ViewHolder> {

    private ArrayList<Restaurant> restaurants;
    private Context context;

    public RestaurantRecyclerViewAdapter(Context context, ArrayList<Restaurant> restaurants) {
        this.restaurants = restaurants;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int i) {


        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_restaurant, parent, false);
        return new RestaurantRecyclerViewAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int i) {

        Restaurant restaurant = restaurants.get(i);
        viewHolder.tvRestaurantName.setText(restaurant.getRestaurantName());

    }

    @Override
    public int getItemCount() {
        return restaurants.size();
    }



    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public TextView tvRestaurantName;

        public ViewHolder(View itemView) {
            super(itemView);

            itemView.setOnClickListener(this);
            this.tvRestaurantName = (TextView) itemView.findViewById(R.id.textRestaurantName);
        }

        @Override
        public void onClick(View v) {

            int position = getLayoutPosition();
            Restaurant restaurant = restaurants.get(position);


            Toast.makeText(context, "Loading Maps for " + restaurant.getRestaurantName(), Toast.LENGTH_SHORT).show();

            Latlng latLng = restaurant.getLatlng();
            Double lat = latLng.getLat();
            Double lng = latLng.getLng();

            Intent mapIntent = new Intent(context, MapsActivity.class);
            mapIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            mapIntent.putExtra("latitude", lat);
            mapIntent.putExtra("longitude", lng);
            context.startActivity(mapIntent);


        }
    }

}
