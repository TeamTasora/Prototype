package com.tasora.ikhthiandor.prototype;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.tasora.ikhthiandor.prototype.adapter.RestaurantRecyclerViewAdapter;
import com.tasora.ikhthiandor.prototype.api.TasoraApiClient;
import com.tasora.ikhthiandor.prototype.model.Restaurant;

import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;


public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Get list of resta

        Toast.makeText(this, "Fetching list of restaurants", Toast.LENGTH_SHORT).show();

        TasoraApiClient.getTasoraApiClient().listRestaurants(new Callback<List<Restaurant>>() {
            @Override
            public void success(List<Restaurant> restaurants, Response response) {
                RecyclerView rvRestaurants = (RecyclerView) findViewById(R.id.rvRestaurants);
                RestaurantRecyclerViewAdapter rrvAdapter = new RestaurantRecyclerViewAdapter(getApplicationContext(), (ArrayList<Restaurant>) restaurants);
                rvRestaurants.setAdapter(rrvAdapter);
                rvRestaurants.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
                rvRestaurants.addItemDecoration(new VerticalSpaceItemDecoration(20));

            }

            @Override
            public void failure(RetrofitError error) {


            }
        });


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

}
